# Layout builder categories

The purpose of this module is to provide adjustments to the handling of the Layout builder Choose Block form that some might find useful.

Currently the module just collapses all categories.