<?php

namespace Drupal\layout_builder_categories\EventSubscriber;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ControllerDecoratorSubscriber implements EventSubscriberInterface {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Construct the ControllerDecoratorSubscriber
   *
   * @param RequestStack $requestStack
   */
  public function __construct(RequestStack $requestStack) {
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::CONTROLLER => ['onController', 0],
    ];
  }

  /**
   * Decorate the ChooseBlock controller.
   *
   * @param \Symfony\Component\HttpKernel\Event\ControllerEvent $event
   *   The controller event.
   */
  public function onController(ControllerEvent $event) {
    $request = $event->getRequest();
    $route_name = $request->attributes->get('_route');

    // Check if the current route is the one you want to target
    if ($route_name === 'layout_builder.choose_block') {
      $original_controller = $event->getController();
      $decorated_controller = [$this, 'decoratedController'];
      $event->setController($decorated_controller);

      // Store the original controller in the request attributes so it can be accessed later
      $request->attributes->set('original_controller', $original_controller);
    }
  }

  /**
   * Decorates ChooseBlockController::build.
   *
   * @return array
   *   A render array.
   */
  public function decoratedController() {
    $request = $this->requestStack->getCurrentRequest();
    $original_controller = $request->attributes->get('original_controller');

    // Call the original (or previously set) controller method
    $build = call_user_func($original_controller, $request);

    // Collapse all categories.
    foreach ($build['block_categories'] as &$category) {
      if (isset($category['#open'])) {
        $category['#open'] = FALSE;
      }
      
    }

    return $build;
  }

}